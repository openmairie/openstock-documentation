.. _reglement:

####################
Facture en reglement
####################

les factures en réglements attendent d'être soldée par le traitement de "solde facture régie"
lorsque les paiements de factures sont apportés à la banque (entreprise) ou au receveur (organisme public)


Saisie facture en reglement
===========================

Il est possible de modifier les reglements dans le menu sortie -> facture en réglement

Seules les factures en réglement sont inclues dans la régie lors du traitement en regie.

Le traitement par la régie des factures en "reglement" permet de basculer en solde toutes les
factures pour une régie donnée (voir regie)


Les actions 
===========

les actions suivantes peuvent être effectuées

.. image:: img/action_facture_reglement.png


Il n'est possible que de saisir l'état de la facture (edition ou solde)

Une facture soldée manuellement n'est pas reprise en régie.


Il est possible d'éditer la facture avec le ou les réglements

.. image:: img/action_edition_facture_versement.png
