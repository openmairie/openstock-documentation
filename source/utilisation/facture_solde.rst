.. _facture_solde:

##############
Facture soldée
##############

Il est possible de modifier les factures soldées dans le menu sortie -> facture soldée

les factures soldées peuvent être archivées au prochain traitement archivage

Les actions possibles sont les suivantes:

.. image:: img/action_facture_transfert.png



Transfert en archive de la facture soldée
=========================================

Ce traitement :

- archive les sorties de prestations et d'artcles en table archive_sortie

- archive la facture et place l'édition dans le dossier facture visible dans le dossier client ou dans le menu archive option facture

En appuyant sur transfert le message de validation apparait :

.. image:: img/confirmation.png

En validant, le message suivant apparait :

.. image:: img/action_transfert_facture.png






