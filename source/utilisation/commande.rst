.. _commande:

########
Commande
########

Saisie des commandes
====================

Il est possible de modifier les commandes dans le menu entree ou dans
le sous formulaire de fournisseur.


.. image:: img/tab_commande.png

En appuyant sur modification :

.. image:: img/form_commande.png


L'utilisateur doit saisir :

- libelle de la commande

- la date

- le fournisseur


le solde de la commande permet de la prendre en compte dans la procédure d'archivage.

les actions suivantes sont alors accessibles :

.. image:: img/action_commande.png

Un état commande et livraison est disponnible sur l'affichage des commandes.

.. image:: img/edition_commande.png

Les lignes de commandes
=======================

Il est possible de mettre à jour les lignes de commandes pour les articles concernés dans le formulaire.

.. image:: img/tab_lignecommande.png

En appuyant sur modification :

le champ "search_article" permet de rechercher un article  du stock (tapez  au moins 2 caractères) et le champ
"search_catalogue" permet de rechercher un article dans le catalogue du fournisseur.

.. image:: img/form_lignecommande1.png

Le message suivant apparait après l'ajout de la saisie

.. image:: img/form_lignecommande2.png






les livraisons
==============

Il est possible de saisir les quantités livrées pour les articles commandés
dans le formulaire en modification.


.. image:: img/form_lignecommande3.png

Attention, il faut avoir créé le bon de livraison

Lors de la livraison les articles sont entrés dans le stock.

.. image:: img/form_lignecommande4.png

Une entrée en livraison  ne peut pas être modifiée mais elle peut être supprimée.
On peut la supprimer ans le sous formulaire "article en livraison" du formulaire
livraison (option "Détail livraison" du menu "Entree")

Liste des articles en livraison

Lorsque la livraison est complète, il n'est plus possible de modifier ou
supprimer une ligne de commande sans supprimer l'entré dans le stock de la livraison
(voir paragraphe suivant)


.. image:: img/tab_lignecommande2.png

Lors de la suppression d'une entrée dans le stock, elle est retirée du stock pour
l'article concerné

