.. _client:

######
Client
######

Client est utile en gestion commerciale par facture


Saisie des clients
==================

Il est possible de modifier les clients dans le menu sortie

.. image:: img/tab_client.png

En appuyant sur modification :

.. image:: img/form_client.png


L'utilisateur doit saisir :

- nom et prénom du client

- l'adresse

L'adresse est en lien avec la base d'adresse nationale et il est récupéré
le cp, la ville et les coordonnées géographiques.

Il est possible d'accéder à une géolocalisation sur une carte en appuyant sur le bouton suivant :

.. image:: img/geom.png

On obtient la carte suivante :

.. image:: img/sig_client.png

Les factures du client
======================

Il est possible de consulter les factures du client non archivées dans le sous formulaire

.. image:: img/tab_client_facture.png

Les devis du client
===================

Il est possible de consulter les devis du client non archivées dans le sous formulaire

.. image:: img/tab_client_devis.png

Dossier
=======

Il est possible de consulter les factures non archivées dans le sous formulaire dossier :

.. image:: img/tab_dossier_client.png

et les modifier ;

.. image:: img/form_dossier_client.png
