.. _archive_regie:

##################
archive des regies
##################



Il est possible de visualiser les regies archivées dans le menu archive

.. image:: img/tab_archive_regie.png

Il est possible de les supprimer

.. image:: img/form_archive_regie.png

L'édition de la regie et des factures associées sont visibles dans l'onglet dossier :

.. image:: img/tab_dossier_regie.png

Il est possible de le supprimer

.. image:: img/form_dossier_regie.png

Attention le dossier et archive regie sont indépendants (pas d'intégrité avec une clé secondaire)
