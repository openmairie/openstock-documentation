.. _archive:


Les archives sont accessibles dans le menu -> archive

- aux entrées articles archivées : liste des commandes et livraisons archivées suite au traitement d archivage

- aux sorties articles et prestations archives suite au traitement d archivage facture et regroupement

- aux livraisons et aux commandes

- aux factures, aux régies et au regroupement.


.. toctree::

    archive_entre.rst
    archive_sortie.rst
    archive_livraison.rst
    archive_commande.rst
    archive_facture.rst
    archive_regie.rst
    archive_regroupement.rst
    