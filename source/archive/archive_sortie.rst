.. _archive_sortie:

###################
archive des sorties
###################



Il est possible de visualiser les sorties archivées dans le menu archive

.. image:: img/tab_archive_sortie.png

Il est possible de les supprimer

.. image:: img/form_archive_sortie.png
