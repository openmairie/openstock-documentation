.. _archive_regroupement:

#########################
archive des regroupements
#########################



Il est possible de visualiser les regroupements archivées dans le menu archive

.. image:: img/tab_archive_regroupement.png

Il est possible de les supprimer

.. image:: img/form_archive_regroupement.png

L'édition de la regroupement  est visible dans l'onglet dossier :

.. image:: img/tab_dossier_regroupement.png

Il est possible de le supprimer

.. image:: img/form_dossier_regroupement.png

ou de le modifier :

.. image:: img/form_dossier_regroupement2.png


Attention le dossier et archive regroupement sont indépendants (pas d'intégrité avec une clé secondaire)
