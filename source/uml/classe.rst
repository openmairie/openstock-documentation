.. _classe:

###################
Diagramme de classe
###################

La présentation des diagrammes de classe permet de comprendre le
fonctionnement global au travers des classes de l'application, de leurs propriétés
et de leurs méthodes

Les procédures stockés et triggers postgres
===========================================

Les méthodes pré fixées par "pr" dans les diagrammes sont des procédures stockées dans postgres 
(voir data/init_metier_trigger.sql)

Ces triggers concernent essentiellement le calcul des stocks (entrées et sorties).

Les cumuls sont gérés avec des vues.

Les packages
============

Il est décrit ci dessous les packages et les liens entre eux

.. image:: openstock.png


Package Article
===============

.. image:: article_classe.png

Package prestation
==================

.. image:: prestation_classe.png

Package commande / livraison
============================

Ce package concerne les entrés dans le stock.

.. image:: entre_classe.png


Package devis
=============

Ce package permet d'établir des devis.

.. image:: devis_classe.png

Package facture
===============

Ce package se référe à la facturation

.. image:: facture_classe.png

Package regroupement
====================

Les sorties peuvent aussi se faire par un regroupement

.. image:: regroupement_classe.png

Package planning
================

Ce package permet de proposer un planning de réalisation d'un bon de commande (facture en cours)

.. image:: article_classe.png

