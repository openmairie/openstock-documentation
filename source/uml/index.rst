.. _uml:


Il est proposé l'analyse UML de l'application dans ce chapitre avec les diagrammes de classes et de transition.

.. toctree::

    classe.rst
    etat_transition.rst
