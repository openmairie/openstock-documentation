.. _tva:

===
TVA
===

Il s'agit de la TVA utilisé en article ou prestation.

L'accès en saisie se fait par le menu paramétrage -> tva :

.. image:: img/tab_tva.png

En saisie 

.. image:: img/form_tva.png

Dans la version 4.0.0 la tva est un pourcentage.
