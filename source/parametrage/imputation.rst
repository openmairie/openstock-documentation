.. _imputation:

##########
Imputation
##########

Il s'agit des imputations de recette de la comptabilité générale (entreprise) ou du budget
de fonctionnement (organisme public)


Saisie des imputations
======================

Accès via le menu paramètrage

.. image:: img/tab_imputation.png

La saisie se fait dans le formulaire suivant :

.. image:: img/form_imputation.png

Les imputations sont affectés aux catégories et permettent la ventilation comptable lors des versements en régie.



