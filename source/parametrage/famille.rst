.. _famille:

=======
Famille
=======

Il s'agit de décrire les familles d'articles


L'accès en saisie se fait par le menu paramétrage -> famille :

.. image:: img/tab_famille.png

En saisie

.. image:: img/form_famille.png

L'onglet article permet d'accéder aux articles de la famille (non modifiable)

Les actions :
=============

.. image:: img/action_famille.png

L'action de contrôle de stock se fait par famille et permet de vérifier qu il
n'est pas atteint le stock minimum.


.. image:: img/action_famille_controle_stock.png

