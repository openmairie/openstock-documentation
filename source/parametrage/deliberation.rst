.. _deliberation:

============
Deliberation
============



Il s'agit des délibérations concernant les tarifs article et prestation (champ non obligatoire)

L'accès en saisie se fait par le menu paramétrage -> deliberation :

.. image:: img/tab_deliberation.png

En saisie

.. image:: img/form_deliberation.png

