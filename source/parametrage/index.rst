.. _parametrage:



Il est proposer de décrire le paramétrage de l'application.

Le menu parametrage est accessible dans le menu :

.. image:: img/menu_parametrage.png


.. toctree::

    om_parametre.rst
    varinc.rst
    imputation.rst
    categorie.rst
    famille.rst
    article.rst
    prestation.rst
    bible.rst
    categorie_ressource.rst
    tva.rst
    unite.rst
    conseiller.rst
    deliberation.rst
