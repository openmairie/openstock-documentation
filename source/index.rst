.. opencimetiere documentation master file, created by
   sphinx-quickstart on Wed Jul 20 17:30:38 2011.

===========================
openStock 4.x documentation
===========================

.. note::

    Cette création est mise à disposition selon le Contrat Paternité-Partage des
    Conditions Initiales à l'Identique 2.0 France disponible en ligne
    http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California 94105, USA.


openStock est une gestion commerciale qui a pour objet de gérer les factures et les stocks
d'une organisation (facturation externe) ou d'un service/regroupement (facturation interne) 


openStock permet :

- le suivi des stocks par la méthode du PMP (Prix Moyen Pondéré): commandes, livraisons, entrées et sorties d'articles

- de faire des devis : article + prestations

- de faire des bons de commandes ou de transformer les devis en bon de commande

- de planifier des interventions

- d'éditer les factures

- de gérer les versements totaux ou partiel suivant plusieurs méthodes : régie directe, titre de recette ou paiement à la recette municipale

- d'archiver les écritures d'entrée d'article  et sortie d'article et de prestation

Depuis la version 3.0.0, openStock fonctionne avec postgresql, ce qui permet de profiter des avantages du transactionel
des triggers et des procédures stockées (version 4.0.0)dans les traitements et d'avoir ainsi une meilleure stabilité.

La version 4.0.0 rajoute le planning (librairie fullcalendar) et la géolocalisation des clients (API de la base nationale d'adresse
et librairie openLayers).

Pour les champs textes, il est utilisé dans la version 4.0.0 la librairie timymce (évolution framework openMairie).

Vous trouverez dans ce document :

- les principes d'ergonomie

- l'analyse UML

- l'utilisation 

- le planning

- le paramétrage

- les traitements

- les archives

Ce document a pour but de guider les utilisateurs et les développeurs dans la
prise en main du projet.


Bonne lecture et n'hésitez pas à nous faire part de vos remarques à l'adresse
suivante : contact@openmairie.org!

Les sources et le téléchargement du projet sont sur la forge de l'ADULLACT au lien suivant :

http://adullact.net/projects/openstock/

Ergonomie
=========

.. toctree::

   ergonomie/index.rst

Analyse UML
===========

.. toctree::

   uml/index.rst


Utilisation
===========

.. toctree::

   utilisation/index.rst


Planning
========

.. toctree::

   planning/index.rst

Paramètrage 
===========

.. toctree::

   parametrage/index.rst

Traitement
==========

.. toctree::

   traitement/index.rst

Archive
=======

.. toctree::

   archive/index.rst

Migration
=========

.. toctree::

   migration/index.rst


Bibliographie
=============

* http://media.readthedocs.org/pdf/omframework/4.8/omframework.pdf



Contributeurs
=============

(par ordre alphabétique)

* François Raynaud (arles)
* karim Saadoune

